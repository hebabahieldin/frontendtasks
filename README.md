**How To Use The Application**

Please follow the steps below:

1. Run 'npm install' to install all the needed node modules

2. Run 'npm start' or 'ng serve' to run the application

3. Navigate to 'http://localhost:4200' from your browser

## Application Overview

An application that has 2 roles:

1. You can login to the system as a System Admin using this email: "george.bluth@reqres.in"

2. You can login to the system as a Normal User using this email: "eve.holt@reqres.in" or any other user from this API "https://reqres.in/api/users"

The application is for users to create and browse posts and related data such as
comments.

User Stories:

* I can register as a new user - FormValidation
API: https://reqres.in/api/register

* I can login using dummy credentials - FormValidation
API: https://reqres.in/api/login

* As an Admin, I should be able to access an Admin Dashboard - Guards
(you can assume for example, user with ID=1 is the admin;
Such check should be made for the user to be able to access the portal)

* Admin dashboard should include Users Management (full CRUD operations)
API: https://reqres.in/ or https://jsonplaceholder.typicode.com/
(Create, Update 'PUT', Update 'PATCH', Delete endpoints in the link)

* As a user I can browse my posts
API: https://jsonplaceholder.typicode.com/users/1/posts

* As a user, I can view comments in each post in a modal
API:
https://jsonplaceholder.typicode.com/posts/1/comments

* As a user I can browse different albums
API: https://jsonplaceholder.typicode.com/albums=1

* As a user, I can browse photos of each album in a separate page
API: https://jsonplaceholder.typicode.com/photos?albumId=1
