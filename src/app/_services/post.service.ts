import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoggedUser } from '../_model/loggeduser';
import { Post } from '../_model/post';



@Injectable({
  providedIn: 'root'
})
export class PostService {
  private currentUserSubject: BehaviorSubject<LoggedUser>;
  public currentUser: Observable<LoggedUser>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<LoggedUser>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): LoggedUser {
    return this.currentUserSubject.value;
  }


  getAllPostsByUserID(id: number) {
    return this.http.get<Post[]>(`https://jsonplaceholder.typicode.com/users/${id}/posts`);
  }

  getAllCommentsByPostID(id: number) {
    return this.http.get<Comment[]>(`https://jsonplaceholder.typicode.com/posts/${id}/comments`);
  }

}
