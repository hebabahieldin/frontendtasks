import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../_model/user';
import { LoggedUser } from '../_model/loggeduser';
import { SingleUser, UserData } from '../_model/singleUser';
import { ListOfUsers } from '../_model/listOfUsers';



@Injectable({
  providedIn: 'root'
})
export class UserService {
  public cUser: LoggedUser;
  private currentUserSubject: BehaviorSubject<LoggedUser>;
  public currentUser: Observable<LoggedUser>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<LoggedUser>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): LoggedUser {
    return this.currentUserSubject.value;
  }

  register(user: User) {
    return this.http.post(`https://reqres.in/api/register`, user);
  }


  //I used the registeration api as it's response got the id of the user and I need it to retrieve the posts of each user as the login api's response is only the token
  login(username, password) {
    return this.http.post<any>(`https://reqres.in/api/register`, { username, password })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        this.getUserById(user.id).subscribe(loggedUser => {
          user.email = loggedUser.data.email;
          user.first_name = loggedUser.data.first_name;
          user.last_name = loggedUser.data.last_name;
          user.avatar = loggedUser.data.avatar;
        });
        this.currentUserSubject.next(user);
        localStorage.setItem('currentUser', JSON.stringify(user));
      }));
  }

  createUser(user: UserData) {
    return this.http.post(`https://reqres.in/api/users`, user);
  }


  updateUser(id: number, user: UserData) {
    return this.http.put(`https://reqres.in/api/users/${id}`, user);
  }


  deleteUser(id: number) {
    return this.http.delete(`https://reqres.in/api/users/${id}`);
  }

  getAll(currentPageNumber: number) {
    return this.http.get<ListOfUsers>(`https://reqres.in/api/users/?page=${currentPageNumber}`);
  }

  getUserById(id: number) {
    return this.http.get<SingleUser>(`https://reqres.in/api/users/${id}`);
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

}
