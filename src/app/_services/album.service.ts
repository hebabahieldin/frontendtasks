import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoggedUser } from '../_model/loggeduser';
import { Post } from '../_model/post';
import { Album } from '../_model/album';
import { Photo } from '../_model/photo';



@Injectable({
  providedIn: 'root'
})
export class AlbumService {
  private currentUserSubject: BehaviorSubject<LoggedUser>;
  public currentUser: Observable<LoggedUser>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<LoggedUser>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): LoggedUser {
    return this.currentUserSubject.value;
  }


  getAllAlbums() {
    return this.http.get<Album[]>(`https://jsonplaceholder.typicode.com/albums`);
  }

  getAllPhotosByAlbumID(id: number) {
    return this.http.get<Photo[]>(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`);
  }

}
