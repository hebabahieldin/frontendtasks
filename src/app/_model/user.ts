export class User {
  id: number;
  email: string;
  password: string;
  name: string;
  first_name: string;
  last_name: string;
  job: string;
  avatar: string;
}
