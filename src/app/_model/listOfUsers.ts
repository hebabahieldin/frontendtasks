import { User } from "./user";

export class Support {
  text: string;
  url: string;

}
export class ListOfUsers {
  data: User[];
  page: number;
  per_page: number;
  support: Support;
  total: number;
  total_pages: number;

}
