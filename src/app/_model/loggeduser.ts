import { Role } from "./role";


export class LoggedUser {
  id: number;
  token: string;
  role: Role;
  email: string;
  password: string;
  name: string;
  first_name: string;
  last_name: string;
  job: string;
  avatar: string;
}
