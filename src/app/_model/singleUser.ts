
export class Support {
  url: string;
  text: string;
}

export class UserData {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}
export class SingleUser {
  data: UserData;
  support: Support;
}
