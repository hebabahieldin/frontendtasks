import { Component, OnInit } from '@angular/core';
import { LoggedUser } from '../_model/loggeduser';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  users = [];
  loadPosts: boolean = true;
  loadAlbums: boolean = false;
  postsBtn;
  albumsBtn;

  constructor(private userService: UserService) { }

  ngOnInit() { }

  loadAlbumComponents() {
    this.loadAlbums = true;
    this.loadPosts = false;
    this.postsBtn = document.getElementById("postsBtn").className = "btn btn-outline-primary";
    this.albumsBtn = document.getElementById("albumsBtn").className = "btn btn-primary";
  }

  loadPostComponents() {
    this.loadAlbums = false;
    this.loadPosts = true;
    this.postsBtn = document.getElementById("postsBtn").className = "btn btn-primary";
    this.albumsBtn = document.getElementById("albumsBtn").className = "btn btn-outline-primary";
  }
}
