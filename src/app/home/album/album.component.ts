import { Component, OnInit } from '@angular/core';
import { Album } from 'src/app/_model/album';
import { LoggedUser } from 'src/app/_model/loggeduser';
import { Photo } from 'src/app/_model/photo';
import { UserData } from 'src/app/_model/singleUser';
import { AlbumService } from 'src/app/_services/album.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  loggedUser: LoggedUser;
  currentUser: UserData;
  users = [];
  albums: Album[] = [];
  photos: Photo[] = [];
  image: string;

  constructor(
    private userService: UserService,
    private albumService: AlbumService
  ) { }

  ngOnInit() {
    this.loggedUser = this.userService.currentUserValue;
    this.userService.getUserById(this.loggedUser.id).subscribe(user => {
      this.currentUser = user.data;
    });
    this.getAlbums();

  }

  getAlbums() {
    this.albumService.getAllAlbums().subscribe(albums => {
      this.albums = albums;
    });
  }

  getPhotosByAlbumId(albumId) {
    this.albumService.getAllPhotosByAlbumID(albumId).subscribe(photos => {
      this.photos = photos;
    });
  }

  showSeperateImage(image) {
    this.image = image;

  }

}
