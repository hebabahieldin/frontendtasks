import { Component, OnInit } from '@angular/core';
import { LoggedUser } from 'src/app/_model/loggeduser';
import { Post } from 'src/app/_model/post';
import { UserData } from 'src/app/_model/singleUser';
import { PostService } from 'src/app/_services/post.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  loggedUser: LoggedUser;
  currentUser: UserData;
  users = [];
  posts: Post[] = [];
  comments: Comment[] = [];

  constructor(
    private userService: UserService,
    private postService: PostService,
  ) { }

  ngOnInit() {
    this.loggedUser = this.userService.currentUserValue;
    this.userService.getUserById(this.loggedUser.id).subscribe(user => {
      this.currentUser = user.data;
    });
    this.getUserPosts(this.loggedUser.id);
  }

  getUserPosts(userID) {
    this.postService.getAllPostsByUserID(userID).subscribe(posts => {
      this.posts = posts;
    });
  }

  getPostComments(postID) {
    this.postService.getAllCommentsByPostID(postID).subscribe(comments => {
      this.comments = comments;
    });
  }
}
