import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoggedUser } from './_model/loggeduser';
import { UserData } from './_model/singleUser';
import { UserService } from './_services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Preventia';
  currentUser: LoggedUser;

  constructor(
    private router: Router,
    private userService: UserService
  ) {

    this.userService.currentUser.subscribe(x => {
      this.currentUser = x;
      console.log(this.currentUser);

    });

    console.log(this.currentUser);
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.token === "QpwL5tke4Pnpja7X1";
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['/login']);
  }
}
