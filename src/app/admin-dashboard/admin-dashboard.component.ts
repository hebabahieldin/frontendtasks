import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ListOfUsers } from '../_model/listOfUsers';
import { UserData } from '../_model/singleUser';
import { UserService } from '../_services/user.service';
import { ToastrService } from "ngx-toastr";



@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  loading = false;
  listOfUsers: ListOfUsers;
  newUser: UserData;
  errorMessage;
  submitted: boolean;
  UserForm: FormGroup;
  isCreateUserButton: boolean;
  updateUserButton: boolean;
  userId: number;
  currentPageNumber: number;
  totalPageNumber: number;



  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {

    this.currentPageNumber = 1;

    this.UserForm = this.formBuilder.group({
      avatar: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
    this.loading = true;
    this.getAllUsers();
  }

  deleteUser(userId) {
    this.userService.deleteUser(userId).subscribe({
      next: resp => {
        this.toastr.error("Deleted", "User Deleted Successfully");
        // this.alertService.alertMessage('User Deleted Successfully');
        this.getAllUsers();
      },
      error: error => {
        this.errorMessage = error.message;
        console.error('There was an error!', error);
      }
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.UserForm.controls; }


  createUser() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.UserForm.invalid) {
      return;
    }
    this.userService.createUser(this.UserForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.toastr.success("Created", "New User Created Successfully");
          // this.alertService.alertMessage('New User Created Successfully');
          this.getAllUsers();
        },
        error => {
          this.toastr.error("Error", error.toString());
          this.loading = false;
        });
  }

  updateUser(userId) {
    this.submitted = true;
    if (this.UserForm.invalid) {
      return;
    }

    this.userService.updateUser(userId, this.newUser)
      .pipe(first())
      .subscribe(
        data => {
          this.toastr.warning("Updated", "User Updated Successfully");
          this.getAllUsers();
        },
        error => {
          this.toastr.error("Error", error.toString());
          this.loading = false;
        });
  }

  createOrUpdateUser() {
    if (this.isCreateUserButton) {
      this.createUser();
    }
    else {
      this.updateUser(this.userId)
    }
  }

  getAllUsers() {
    this.userService.getAll(this.currentPageNumber).pipe(first()).subscribe(listOfUsers => {
      this.loading = false;
      this.listOfUsers = listOfUsers;
      this.totalPageNumber = this.listOfUsers.total_pages;
    });
  }

  getNextPageOfUsers() {
    this.currentPageNumber++;
    this.getAllUsers();
  }

  getPreviuosPageOfUsers() {
    this.currentPageNumber--;
    this.getAllUsers();
  }

  showUpdateButton(user) {
    this.isCreateUserButton = false;
    this.userId = user.id;
    this.UserForm.setValue({ avatar: user.avatar, first_name: user.first_name, last_name: user.last_name, email: user.email });

  }

  showCreateButton() {
    this.isCreateUserButton = true;
    this.UserForm.setValue({ avatar: null, first_name: null, last_name: null, email: null });
  }

}
